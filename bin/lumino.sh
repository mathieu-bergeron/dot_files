lumino=1


if [ "$1" != "" ]; then
    lumino=$(echo "scale=2;$1/10" | bc)
fi

screens=$(xrandr  | grep " connected" | cut -d" " -f1)

for screen in $screens;
do
    xrandr  --output "$screen" --brightness "$lumino"
done

