#! /bin/bash

echo ""
echo "Dépôts git non-poussés"
find ~/ -name ".git" -type d 2>/dev/null | while read i; do pushd $(dirname $i) >/dev/null; git status | egrep "avance.*commit" | wc -l | xargs -i test {} -gt 0 && echo $(dirname $i); popd >/dev/null; done
echo ""
