# https://projects.eclipse.org/projects/eclipse.jdt.ls

racine=/home/mbergeron/.local/eclipse_jdt_server
version=org.eclipse.equinox.launcher_1.4.0.v20161219-1356.jar 

java -Declipse.application=org.eclipse.jdt.ls.core.id1 \
	-Dosgi.bundles.defaultStartLevel=4 \
	-Declipse.product=org.eclipse.jdt.ls.core.product \
	-Dlog.protocol=true \
	-Dlog.level=ALL \
	-noverify \
	-Xmx1G \
	-jar $racine/plugins/$version \
	-configuration $racine/config_linux \
	--add-modules=ALL-SYSTEM \
	--add-opens java.base/java.util=ALL-UNNAMED \
	--add-opens java.base/java.lang=ALL-UNNAMED


