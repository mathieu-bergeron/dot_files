# Installation

1. Partitions:
    1. 1G:  boot
    1. 15G: swap
    1. le reste: partition native (pas LVM!)
1. Bspwm
    1. police Terminus:
            $ pacman -S terminus-font
    1. vérifier le nom:
            $ fc-list | grep Terminus
