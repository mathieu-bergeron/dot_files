# TODO

## Fichiers de config

1. Fichiers de config pour zsh
1. Modulariser:
    * fichiers par thème, p.ex.:
        * env_shell
        * a_demarrer_graphique

    * au bon endroit, on fait 

            source ~/.ciboulot/bon_fichier

1. Spécialiser
    * options dans le fichiers, p.ex.
        * par ordi (B3313 - B3317)
        * par session (AndroidStudio Vs Eclipse)


## Config Vim


1. Plugin pour les imports en Java:  https://www.vim.org/scripts/script.php?script_id=325
1. Bogue de rename avec le serveur jdt eclipse

## Config Eclipse / Vim

1. Démarrer le serveur Eclipse et utiliser Vim comme client


## Scriptage des fenêtres

1. Script pour démarrer un *écran de débug* avec p.ex.:
    * trois terminal
    * chaque terminal va lancer un client (qui floate)

